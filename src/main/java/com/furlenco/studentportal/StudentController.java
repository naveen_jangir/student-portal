package com.furlenco.studentportal;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;


@RestController
public class StudentController {

    @Autowired
    private StudentService studentService;

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String index() {
        return "Welcome to Student Portal.";
    }

    @RequestMapping(value = "/students", method = RequestMethod.GET)
    public Response<List<Student>> getAllStudents(@RequestParam(value = "classes", required = false) List<Integer> classIdList,
                                                  @RequestParam(value = "admissionYearAfter", required = false) String admissionYearAfter,
                                                  @RequestParam(value = "admissionYearBefore", required = false) String admissionYearBefore,
                                                  @RequestParam(value = "active", required = false) Boolean active) {
        List<Student> students = studentService.getAllStudents(classIdList, active, admissionYearAfter, admissionYearBefore);
        return new Response<>("200", students);
    }

    @RequestMapping(value = "/students/{studentId}", method = RequestMethod.GET, produces = {"application/json"})
    public Response<Student> getStudent(@PathVariable("studentId") Long id) {
        Student student = studentService.getStudent(id);
        if (student != null) {
            return new Response<>("200", student);
        } else {
            return new Response<>("404", null, "Student not in database.");
        }
    }

    @RequestMapping(value = "/students", method = RequestMethod.POST)
    public Response<Student> addStudent(@RequestBody Student student) {
        if (student.getName() == null || student.getName().equals("")) {
            return new Response<Student>("400", null, "name should not be null or empty");
        }
        if (student.getClassId() == null) {
            return new Response<Student>("400", null, "classId should not be null");
        }
        if (student.getAdmissionYear() == null) {
            return new Response<Student>("400", null, "admissionYear should not be null");
        }
        student.setActive(true);
        studentService.addStudent(student);
        return new Response<Student>("201", null);
    }

    @RequestMapping(value = "/students/{id}", method = RequestMethod.PATCH)
    public Response<Student> updateTopic(@RequestBody Student student, @PathVariable Long id) {
        Student student1 = studentService.updateStudent(student, id);
        return new Response<>("200", student1);
    }

    @RequestMapping(value = "/students/{id}", method = RequestMethod.DELETE)
    public Response<Student> deleteStudent(@PathVariable("id") Long id) {
        Student student = studentService.deleteStudent(id);
        if (student != null)
            return new Response<Student>("200", student);
        else
            return new Response<>("404", null);
    }
}

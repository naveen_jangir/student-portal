package com.furlenco.studentportal;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

@Service
public class StudentService {

    @Autowired
    private StudentRepository studentRepository;

    public List<Student> getAllStudents(List<Integer> classIdList, Boolean active, String admissionYearAfter, String admissionYearBefore) {
        List<Student> students = new ArrayList<>();
        studentRepository.findAll().forEach(students :: add);
        List<Student> classIdStudent = new ArrayList<>();

        for (Student student : students) {
            if (admissionYearAfter != null) {
                Calendar c = Calendar.getInstance();
                c.setTime(student.getAdmissionYear());
                int admissionYear = c.get(Calendar.YEAR);
                int yearAfter = Integer.parseInt(admissionYearAfter);
                if (admissionYear < yearAfter)
                    continue;
            }

            if (admissionYearBefore != null) {
                Calendar c = Calendar.getInstance();
                c.setTime(student.getAdmissionYear());
                int admissionYear = c.get(Calendar.YEAR);
                int yearBefore = Integer.parseInt(admissionYearBefore);
                if (admissionYear > yearBefore)
                    continue;
            }

            if (classIdList != null && classIdList.size() != 0) {
                if (!classIdList.contains(student.getClassId()))
                    continue;
            }
            if (active != null) {
                if (!student.getActive().equals(active)) {
                    continue;
                }
            }
            classIdStudent.add(student);
        }

        students = classIdStudent;
        return students;
    }

    public Student getStudent(Long id) {
        return studentRepository.findById(id).orElse(null);
    }

    public void addStudent(Student student) {
        studentRepository.save(student);
    }


    public Student updateStudent(Student student, Long id) {
        Student student1 = getStudent(id);
        if (student.getClassId() != null)
            student1.setClassId(student.getClassId());
        studentRepository.save(student1);
        return student1;
    }

    public Student deleteStudent(Long id) {
        Student student = getStudent(id);
        if (student != null) {
            student.setActive(false);
            studentRepository.save(student);
//            studentRepository.deleteById(id);
        }
        return student;
    }
}

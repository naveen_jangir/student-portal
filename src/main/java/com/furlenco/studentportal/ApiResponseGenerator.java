//package com.furlenco.studentportal;
//
//import com.google.gson.GsonBuilder;
//import com.shuttl.helpers.utils.ResponseExclusionStrategy;
//import org.apache.commons.lang3.StringUtils;
//import org.springframework.http.CacheControl;
//
//import javax.ws.rs.core.CacheControl;
//import javax.ws.rs.core.MediaType;
//import javax.ws.rs.core.Response;
//
///**
// * @author manishsharma
// *         12/19/15
// *         1:37 PM
// */
//public class ApiResponseGenerator {
//    private static CacheControl getCacheControl() {
//        CacheControl cacheControl = new CacheControl();
//        cacheControl.setMaxAge(0);
//        cacheControl.setNoCache(true);
//        return cacheControl;
//    }
//
//    public static Response generateResponse(Object o) {
//        if (o != null) {
//            return Response.ok(getDefaultGsonBuilder().disableHtmlEscaping().create().toJson(o), MediaType.APPLICATION_JSON_TYPE).status(200).cacheControl(getCacheControl()).build();
//        } else {
//            return Response.noContent().build();
//        }
//    }
//
//    public static Response generateErrorResponse(Object o, int statusCode) {
//        if (o != null) {
//            return Response.ok(getDefaultGsonBuilder().create().toJson(o), MediaType.APPLICATION_JSON_TYPE).status(statusCode).cacheControl(getCacheControl()).build();
//        } else {
//            return Response.noContent().build();
//        }
//    }
//
//    public static Response generateHtmlResponse(String html) {
//        if (StringUtils.isNotBlank(html)) {
//            return Response.ok(html, MediaType.TEXT_HTML_TYPE).status(200).cacheControl(getCacheControl()).build();
//        } else {
//            return Response.noContent().build();
//        }
//    }
//
//    private static GsonBuilder getDefaultGsonBuilder() {
//        GsonBuilder builder = new GsonBuilder();
//        builder.setPrettyPrinting();
//        builder.serializeSpecialFloatingPointValues();
//        builder.addSerializationExclusionStrategy(ResponseExclusionStrategy.getInstance());
//        return builder;
//    }
//}

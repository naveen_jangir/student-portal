//package com.furlenco.studentportal;
//
//import com.shuttl.rest.v2.beans.WarningResponseDTO;
//
//import java.util.List;
//
///**
// * @author manishsharma
// *         12/19/15
// *         1:37 PM
// */
//public class ApiResponse<T> {
//    private boolean success;
//    private T data;
//    private WarningResponseDTO warning;
//    private String errorCode;
//    private String message;
//    private boolean cached;
//    private List<String> errors;
//    private String title;
//
//    public ApiResponse(boolean success, T data, String errorCode, String message, boolean cached, String title) {
//        this.success = success;
//        this.data = data;
//        this.errorCode = errorCode;
//        this.message = message;
//        this.cached = cached;
//        this.title = title;
//    }
//
//    public ApiResponse(boolean success, T data, String errorCode, String message, boolean cached) {
//        this.success = success;
//        this.data = data;
//        this.errorCode = errorCode;
//        this.message = message;
//        this.cached = cached;
//    }
//
//    public ApiResponse(boolean success, T data, WarningResponseDTO warning, String errorCode, String message, boolean cached, String title) {
//        this.success = success;
//        this.data = data;
//        this.warning = warning;
//        this.errorCode = errorCode;
//        this.message = message;
//        this.cached = cached;
//        this.title = title;
//    }
//
//    public ApiResponse(T data, String message) {
//        this.data = data;
//        this.message = message;
//    }
//
//    public static <T> ApiResponse<T> failedResponse(String errorCode, String errorMessage) {
//        return new ApiResponse<T>(false, null, errorCode, errorMessage, false, null);
//    }
//
//    public static <T> ApiResponse<T> failedResponse(WarningResponseDTO warning, String errorCode, String errorMessage) {
//        return new ApiResponse<T>(false, null, errorCode, errorMessage, false, null);
//    }
//
//    public static <T> ApiResponse<T> failedResponse(WarningResponseDTO warning, String errorCode, String errorMessage, String errorTitle) {
//        return new ApiResponse<T>(false, null, warning, errorCode, errorMessage, false, errorTitle);
//    }
//
//    public static <T> ApiResponse<T> failedResponse(String errorCode, String errorMessage, String errorTitle) {
//        return new ApiResponse<T>(false, null, errorCode, errorMessage, false, errorTitle);
//    }
//
//    public static <T> ApiResponse<T> failedResponse(T data, String errorCode, String errorMessage) {
//        return new ApiResponse<T>(false, data, errorCode, errorMessage, false, null);
//    }
//
//    public static <T> ApiResponse<T> failedResponse(T data, String errorCode, String errorMessage, String errorTitle) {
//        return new ApiResponse<T>(false, data, errorCode, errorMessage, false, errorTitle);
//    }
//
//    public static <T> ApiResponse<T> successResponse(T data, boolean cached) {
//        return new ApiResponse<T>(true, data, null, null, cached, null);
//    }
//
//    public static <T> ApiResponse<T> successResponse(T data, boolean cached, String title) {
//        return new ApiResponse<T>(true, data, null, null, cached, title);
//    }
//
//    public static <T> ApiResponse<T> successResponse(T data, String message) {
//        return new ApiResponse<T>(data, message);
//    }
//
//    public static <T> ApiResponse<T> successResponse(T data, String message, boolean cached) {
//        return new ApiResponse<T>(true, data, null, message, cached, null);
//    }
//
//    public static <T> ApiResponse<T> successResponse(T data, String message, boolean cached, String title) {
//        return new ApiResponse<T>(true, data, null, message, cached, title);
//    }
//
//    public boolean isSuccess() {
//        return success;
//    }
//
//    public void setSuccess(boolean success) {
//        this.success = success;
//    }
//
//    public T getData() {
//        return data;
//    }
//
//    public void setData(T data) {
//        this.data = data;
//    }
//
//    public String getErrorCode() {
//        return errorCode;
//    }
//
//    public void setErrorCode(String errorCode) {
//        this.errorCode = errorCode;
//    }
//
//    public String getMessage() {
//        return message;
//    }
//
//    public void setMessage(String message) {
//        this.message = message;
//    }
//
//    public boolean isCached() {
//        return cached;
//    }
//
//    public void setCached(boolean cached) {
//        this.cached = cached;
//    }
//
//    public List<String> getErrors() {
//        return errors;
//    }
//
//    public void setErrors(List<String> errors) {
//        this.errors = errors;
//    }
//
//    public String getTitle() {
//        return title;
//    }
//
//    public void setTitle(String title) {
//        this.title = title;
//    }
//
//    public WarningResponseDTO getWarning() {
//        return warning;
//    }
//
//    public void setWarning(WarningResponseDTO warning) {
//        this.warning = warning;
//    }
//
//    @Override
//    public String toString() {
//        return "ApiResponse{" +
//                "success=" + success +
//                ", data=" + data +
//                ", warning=" + warning +
//                ", errorCode='" + errorCode + '\'' +
//                ", message='" + message + '\'' +
//                ", cached=" + cached +
//                ", errors=" + errors +
//                ", title='" + title + '\'' +
//                '}';
//    }
//}

package com.furlenco.studentportal;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.util.Date;

@JsonSerialize()
@Entity(name = "students")
public class Student {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name = "class_id")
    private Integer classId;
    private String name;
//    @Type(type = "org.hibernate.type.NumericBooleanType")
    private Boolean active;
    @Column(name = "admission_year")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/mm/yyyy")
    private Date admissionYear;

    public Student() {
    }

    public Student(Long id, Integer classId, String name, Boolean active) {
        this.id = id;
        this.classId = classId;
        this.name = name;
        this.active = active;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getClassId() {
        return classId;
    }

    public void setClassId(Integer classId) {
        this.classId = classId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public Date getAdmissionYear() {
        return admissionYear;
    }

    public void setAdmissionYear(Date admissionYear) {
        this.admissionYear = admissionYear;
    }

    @Override
    public String toString() {
        return "Student{" +
                "id=" + id +
                ", classId=" + classId +
                ", name='" + name + '\'' +
                ", active=" + active +
                ", admissionYear=" + admissionYear +
                '}';
    }
}
